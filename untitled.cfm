<cffunction name="imageUpload" access="public" returntype="String">
		<cfargument name="productName" type="string" required="true">
		<cfargument name="productId" type="numeric" required="true">
		<cfargument name="imagefile" required="true">
		<cftry>

			<!--- #### To generate a random number ####--->
			<cfset var randomNumber =  CreateUUID() >
			<!--- #### To create a unique filename ####--->
			<cfset  variables.fileName = #arguments.productName# & '_' & #arguments.productId# & '_' & #randomNumber#/>
			<!--- #### Check if File Exists ####--->
			<cfloop condition ="FileExists('/image/product-img/#variables.fileName#') EQ TRUE ">
				<!--- #### If file exist create a unique file name ####--->
				<cfset randomNumber =  CreateUUID() >
				<cfset  variables.fileName = #arguments.productName# &'_' & #arguments.productId# & '_' & #randomNumbe#/>
			</cfloop>
			<cfset variables.imagepath = ExpandPath("/") & "image\#variables.fileName#\" & variables.fileName & ".jpg"/>
			<!--- Uploading the image to the server --->
			<cffile action="upload" accept="image/jpeg" filefield = "#imagefile#" destination="#variables.imagepath#" result="uploadResult" >
			<!--- Returning the unique file name with path --->
			<cfreturn "image/#variables.fileName#/" & variables.fileName & ".jpg">
			<cfcatch type="any">
				<cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:imageUpload">
			</cfcatch>
		</cftry>
	</cffunction>


