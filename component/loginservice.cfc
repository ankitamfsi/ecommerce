<!---
  --- loginservice
  --- ------------
  ---
  --- author: ankita
  --- date:   3/12/19
  --->
<cfcomponent>

	<!---Handling data for succesful Login--->

			<cffunction name="dbHandler" access="public" returntype="Numeric">
			<cfargument name="form" type="struct" required="true">
			<cfset var check = 0/>
			<cfquery name="checkLogin">

				SELECT Salt, Password, Name, Role
			 	FROM Person
			 	WHERE Email = <cfqueryparam value="#arguments.form.loginID#" cfsqltype="cf_sql_varchar"/>
			</cfquery>

			<cfif checkLogin.RecordCount NEQ 0>
				<cfset var salt = checkLogin["Salt"]/>
				<cfset var queryPassword = checkLogin["Password"]/>

				<cfset var verifyPassword = Hash(form.loginPassword & salt, "SHA-512")/>

				<cfif queryPassword EQ verifyPassword>
					<!---Session creation for Logged In User--->
					<cfset session.loggedInUser = {'name'= checkLogin.Name, 'role'= checkLogin.Role}/>
				<cfelse>
					<cfset check = 1/>
				</cfif>
			</cfif>
			<cfreturn check>
		</cffunction>


		<!---For deletion of session after logout--->

		<cffunction name="sessionlogout">
			<cfset StructDelete(session, "loggedInUser")/>
			<cfset StructDelete(cookie,"cfid")/>
			<cfset StructDelete(cookie,"cftoken")/>
			<cfreturn 1>
		</cffunction>


</cfcomponent>