<!---
  --- addproduct
  --- ----------
  ---
  --- author: ankita
  --- date:   3/15/19
  --->
<cfcomponent>

	<cffunction name = "addProduct" access = "public" returntype = "Numeric">
		<cfargument name = "form" type = "struct" required = "true">
		<cfset var check = 1/>
			<cfquery datasource="mystore" name="formQuery">
				SELECT Name
				FROM Product
				WHERE Name = <cfqueryparam value= #arguments.form.productName# cfsqltype="cf_sql_varchar"/>
			</cfquery>
			<cfif formQuery.RecordCount EQ 0>
				<cfset check = 0/>
				<cfset variables.categoryResult = ""/>
				<cfquery datasource="mystore" name="formQuery">
					SELECT Category_ID
					FROM Product_Category
					WHERE Category = <cfqueryparam value= #arguments.form.category# cfsqltype="cf_sql_varchar"/>
				</cfquery>
				<cfif formQuery.RecordCount EQ 0>
					<cfquery result="categoryDetails">
						INSERT INTO Product_Category (Category)
						VALUES
						(
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.form.category#" />
						)
					</cfquery>
					<cfset categoryResult = #categoryDetails.generatedKey#>
				<cfelse>
						<cfset categoryResult = formQuery["Category_ID"]>
				</cfif>
				<cfset variables.subcategoryResult = ""/>
				<cfquery datasource="mystore" name="formQuery">
					SELECT Sub_Category_ID
					FROM Product_SubCategory
					WHERE Sub_Category = <cfqueryparam value= #arguments.form.subcategory# cfsqltype="cf_sql_varchar"/>
				</cfquery>
				<cfif formQuery.RecordCount EQ 0>
					<cfquery result="subcategoryDetails">
						INSERT INTO Product_SubCategory (Sub_Category,Category_ID)
						VALUES
						(
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.form.subcategory#" />,
							<cfqueryparam cfsqltype="CF_SQL_INTEGER" value = "#categoryResult#" />
						)
					</cfquery>
					<cfset subcategoryResult = #subcategoryDetails.generatedKey#>
				<cfelse>
						<cfset subcategoryResult = formQuery["Sub_Category_ID"]>
				</cfif>
				<cfquery datasource="mystore" name="formQuery">
					INSERT INTO Product (Name, Description, Sub_Category_ID, Product_Price, Product_Img)
					VALUES
					(
						<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value = "#arguments.form.productName#" />,
						<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value = "#arguments.form.productDescription#" />,
						<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value = "#subcategoryResult#" />,
						<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value = "#arguments.form.productPrice#" />,
						<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value = "#arguments.form.imageupload#" />
					)
				</cfquery>
			</cfif>
			<cfreturn check>

	</cffunction>

	<!---Function to handle the new category to be added--->
	<cffunction name = "addcategory" access = "public" returntype = "Numeric">
		<cfargument name = "form" type = "string" required = "true">
			<cfset var check = 0/>
			<cfquery name="getCategory">
				SELECT Category_ID
				FROM Product_Category
				WHERE Category = <cfqueryparam value= #arguments.form.categoryName# cfsqltype="cf_sql_varchar"/>
			</cfquery>
			<cfif getCategory.RecordCount EQ 0>
			  <cfquery name="newCategory">
				  INSERT INTO Product_Category (Category)
						VALUES
						(
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.form.categoryName#" />
						)
			  </cfquery>
			  <cfset check = 1/>
			 </cfif>
	</cffunction>

	<!---Function to handle the new sub-category to be added--->
	<cffunction name = "addsubcategory" access = "public" returntype = "Numeric">
		<cfargument name = "subcategoryname" type = "string" required = "true">
		<cfargument name = "categoryid" type = "string" required = "true">
			<cfset var check = 0/>
			<cfquery name="getSubCategory">
				SELECT Sub_Category_ID
				FROM Product_SubCategory
				WHERE Sub_Category = <cfqueryparam value= #arguments.form.subcategoryName# cfsqltype="cf_sql_varchar"/>
			</cfquery>
			<cfif getCategory.RecordCount EQ 0>
			  <cfquery name="newSubCategory">
				  INSERT INTO Product_SubCategory (Sub_Category, Category_ID)
						VALUES
						(
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.form.subcategoryName#" />
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.form.selectCategory#" />
						)
			  </cfquery>
			  <cfset check = 1/>
			 </cfif>
	</cffunction>

<!--- Function to send category details through AJAX --->
<cffunction name = "getCategory" access = "public" returntype="any">
	<cfset var categoryDetails = []/>
	<cfquery name="getCategoryDetails">
		SELECT Category, Category_ID
		FROM Product_Category
	</cfquery>
		<cfset var i = 1/>

		<cfloop query = "getCategoryDetails">
			<cfset categoryDetails[i]['categoryName'] = #getCategoryDetails.Category# />
			<cfset categoryDetails[i]['categoryId'] = #getCategoryDetails.Category_ID# />

			<cfset i++ />
		</cfloop>

		<cfreturn categoryDetails>
</cffunction>

<!--- Function to send sub category details through AJAX --->
<cffunction name = "getSubCategory" access = "remote" returntype="any">
	<cfset var subcategoryDetails = []/>
	<cfquery name="getSubCategoryDetails">
		SELECT Sub_Category, Sub_Category_ID
		FROM Product_SubCategory
	</cfquery>
		<cfset var i = 1/>

		<cfloop query = "getSubCategoryDetails">
			<cfset subcategoryDetails[i]['subcategoryName'] = #getSubCategoryDetails.Sub_Category# />
			<cfset subcategoryDetails[i]['subcategoryId'] = #getSubCategoryDetails.Sub_Category_ID# />

			<cfset i++ />
		</cfloop>
		<cfreturn subcategoryDetails>
</cffunction>

<cffunction name = "handleCategories" access = "remote" returntype="any">
	<cfargument name = "categoryType" type = "string" required = "true">
	<cfset var categoryDetails = []/>
	<cfquery name="handleSubCategoryDetails">
		SELECT Sub_Category, Sub_Category_ID
		FROM Product_SubCategory
		WHERE Category_ID = <cfqueryparam value= #arguments.categoryType# cfsqltype="cf_sql_varchar"/>
	</cfquery>
	<cfset var i = 1/>

	<cfloop query = "handleSubCategoryDetails">
		<cfset categoryDetails[i]['subcategoryName'] = #handleSubCategoryDetails.Sub_Category# />
		<cfset categoryDetails[i]['subcategoryId'] = #handleSubCategoryDetails.Sub_Category_ID# />

		<cfset i++ />
	</cfloop>
	<cfreturn categoryDetails>
</cffunction>

<cffunction name = "addImage" access = "public" returntype = "any">
		<cfargument name="productName" type="string" required="true">
		<cfargument name="imagefile" required="true">
		<cftry>
			<cfif directoryExists("././images/#arguments.productName#")>
				<!---work here--->
				<!--- Generating a random number --->
				<cfset var randomNumber =  CreateUUID() >
				<!--- Creating a unique filename --->
				<cfset  variables.fileName = #arguments.productName# & '_'& '_' & #randomNumber#/>
				<!--- Checking if File Exists --->
				<cfloop condition ="FileExists('/image/#arguments.productName#/#variables.fileName#') EQ TRUE ">
					<cfset randomNumber =  CreateUUID() >
					<cfset  variables.fileName = #arguments.productName# &'_' & '_' & #randomNumber#/>
				</cfloop>
			<cfelse>
				<cfdirectory action="create" directory="#expandPath("././images/#arguments.productName#")#">
				<!--- Generating a random number --->
				<cfset var randomNumber =  CreateUUID() >
				<!--- Creating a unique filename --->
				<cfset  variables.fileName = #arguments.productName# & '_'& '_' & #randomNumber#/>
				<!--- Checking if File Exists --->
				<cfloop condition ="FileExists('/image/#arguments.productName#/#variables.fileName#') EQ TRUE ">
					<cfset randomNumber =  CreateUUID() >
					<cfset  variables.fileName = #arguments.productName# &'_' & '_' & #randomNumber#/>
				</cfloop>
			</cfif>
			<cfset variables.imagepath = ExpandPath("/") & "image\product-img\" & variables.fileName & ".jpg"/>
		<cfcatch type="any" >
			<cfoutput>abc</cfoutput>
		</cfcatch>
		</cftry>
</cffunction>


</cfcomponent>