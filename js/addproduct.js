

var error_productname = false;
var error_productcategory = false;
var error_productsubcategory = false;
var error_productprice = false;
var error_productcategoryname = false;
var error_productsubcategoryname = false;


var productname = $("#productName");
var productprice = $("#productPrice");
var productcategory = document.getElementById("category");
var val1 = productcategory.options[productcategory.selectedIndex];
var productsubcategory = document.getElementById("subcategory");
var val2 = productsubcategory.options[productsubcategory.selectedIndex];
var productcategoryname = $("#categoryName");
var productsubcategoryname = $("#subcategoryName");


function checkCategory(value,error,error_id)
{
	if(value == "Select Category" || value == "Select Sub-Category")
	{
		document.getElementById(error_id).innerHTML = 'Field cannot be empty';
		return false;
	}
	else
	{
		document.getElementById(error_id).innerHTML = '';
	}
	
}

function checkEmptyField(value,error,error_id)
{
	if(value == "")
	{
		document.getElementById(error_id).innerHTML = 'Field cannot be empty';
		return false;
	}
	else
	{
		document.getElementById(error_id).innerHTML = '';
	}
}

function checkPrice(value,error,error_id)
{
	if(value == "")
	{
		document.getElementById(error_id).innerHTML = 'Field cannot be empty';
		return false;
	}
	else
	{
		if(!(/^[0-9]+(\.[0-9]+)?$/.test(value)))
		{
			document.getElementById(error_id).innerHTML = 'Enter valid Price';
			return false;
		}
		else
		{
			document.getElementById(error_id).innerHTML = '';
			return true;
		}
		
		error = true;
	}
	
}

function checkProduct()
{
	checkProductName(productname.val(),error_productname,"productname_error_message");
	checkPrice(productprice.val(),error_productprice,"productprice_error_message");
	checkCategory(val1.value,error_productcategory,"productcategory_error_message");
	checkCategory(val2.value,error_productsubcategory,"productsubcategory_error_message");
	
	if(error_productname == false || error_productprice == false ||
			error_productcategory == false || error_productsubcategory == false)
		{
			return false;
		}
}

function productname_field()
{
	error_productname = checkProductName(productname.val(),error_productname,"productname_error_message");
}

function productprice_field()
{
	error_productprice = checkPrice(productprice.val(),error_productprice,"productprice_error_message");
}

function productcategory_field()
{
	 val1 = productcategory.options[productcategory.selectedIndex];
	error_productcategory = checkCategory(val1.value,error_productcategory,"productcategory_error_message");
}

function productsubcategory_field()
{
	 val2 = productsubcategory.options[productsubcategory.selectedIndex];
	error_productsubcategory = checkCategory(val2.value,error_productsubcategory,"productsubcategory_error_message");
}

function checkCategory()
{
	checkEmptyField(productcategoryname.val(),error_productcategoryname,"categoryname_error_message");
	
	if(error_productcategoryname == false)
		{
			return false;
		}
}

function checkSubCategory()
{
	checkEmptyField(productsubcategoryname.val(),error_productsubcategoryname,"subcategoryname_error_message");
	
	if(error_productsubcategoryname == false)
		{
			return false;
		}
}

function productcategoryname_field()
{
	error_productcategoryname = checkEmptyField(productcategoryname.val(),error_productcategoryname,"categoryname_error_message");
}

function productsubcategoryname_field()
{
	error_subproductcategoryname = checkEmptyField(productsubcategoryname.val(),error_productsubcategoryname,"subcategoryname_error_message");
}

//for managing category types to be displayed to the user in add sub-category modal

$("#exampleModal5").ready(function(){
	  $.ajax
	  ({
		  url: "controller/actionaddproduct.cfm?getCategory",
		  type: "GET",
		  success: function(data)
		  {console.log("ankita");
		   console.log(data);
			  categories = JSON.parse(data);
			  $.each( categories, function( index, value )
			  {
				  $("#selectCategory").append('<option value='+value.categoryId+'>'+
				  value.categoryName+'</option>')
				  });
		  },
		  error: function(data)
		  {
			  console.log(data)
		  }
	  });
	});

//for managing category types to be displayed in the drop down in the add product modal

$("#exampleModal3").ready(function(){
	console.log("here");
	  $.ajax
	  ({
		  url: "controller/actionaddproduct.cfm?getCategories",
		  type: "GET",
		  success: function(data)
		  {console.log(data);
			  categories = JSON.parse(data);
			  $.each( categories, function( index, value )
			  {
				  $("#category").append('<option value='+value.categoryId+'>'+
				  value.categoryName+'</option>')
				  });
		  },
		  error: function(data)
		  {
			console.log("errorHere");
			  console.log(data);
		  }
	  });
	});


// for adding subcategories according to the category selected

$("#category").change(function(){
	 $.ajax
	 ({
		 url: "controller/actionaddproduct.cfm?handleCategories",
		 type: "GET",
		 data: {
             Site: $("#category").val()
             
             },
             
		 success: function(data)
		 {
			 //console.log(data);
			 categories = JSON.parse(data);
			  $.each( categories, function( index, value )
			  {
				  
				  $("#subcategory").append('<option value='+value.subcategoryId+'>'+
				  value.subcategoryName+'</option>')
				  });
		 },
		 error: function(data)
		 {
			 console.log(data);
		 }
	 })
});
