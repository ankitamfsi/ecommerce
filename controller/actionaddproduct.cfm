<cfset validateObject= createObject("component","component.productauthenticate")/>
<cfset dbObject= createObject("component","component.productservice")/>

<cfif structKeyExists(form,"addcategory_button")>

 <!---Function to check if the fields are empty, to avoid insertion of null values--->

	<cfset variables.emptyError = {}/>

	<cfset error = validateObject.checkEmptyField(form.categoryName,"productCategoryError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfif emptyError["productCategoryError"] EQ "">
		<cfset variables.checkvalue = dbObject.addcategory(form.categoryName)/>
	</cfif>

	<cfif check EQ 0>
		<cfoutput>
			<h3 class="error_form">
				Category Already Exists! Please enter a different Category.
			</h3>
		</cfoutput>
	<cfelse>
		<cfoutput>
			<h3 class="error_form">
				Category added!
			</h3>
		</cfoutput>
	</cfif>
</cfif>

<cfif structKeyExists(form,"addsubcategory_button")>

 <!---Function to check if the fields in add-sub-category are empty, to avoid insertion of null values--->

	<cfset variables.emptyError = {}/>

	<cfset error = validateObject.checkEmptyField(form.subcategoryName,"productSubCategoryError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfset error = validateObject.checkEmptyField(form.selectCategory,"productSelectCategoryError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfif emptyError["productSubCategoryError"] EQ "" AND emptyError["productSelectCategoryError"] EQ "">
		<cfset variables.checkvalue = dbObject.addsubcategory(form.subcategoryName,form.selectCategory)/>
	</cfif>

	<cfif checkvalue EQ 0>
		<cfoutput>
			<h3 class="error_form">
				Sub-Category Already Exists! Please enter a different Sub-Category.
			</h3>
		</cfoutput>
	<cfelse>
		<cfoutput>
			<h3 class="error_form">
				Category added!
			</h3>
		</cfoutput>
	</cfif>
</cfif>


<cfif structKeyExists(form,"addproduct_button")>

	<!---Function to check if the fields are empty, to avoid insertion of null values in product table--->

	<cfset variables.emptyError = {}/>

	<cfset error = validateObject.checkEmptyField(form.productName,"productNameError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfset error = validateObject.checkEmptyField(form.category,"productCategoryError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfset error = validateObject.checkEmptyField(form.subcategory,"productSubCategoryError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfset error = validateObject.checkProductPrice(form.productPrice,"productPriceError")/>
	<cfset StructAppend(emptyError,error,true)/>

	<cfset variables.flag = 0/>

	<cfloop collection="#emptyError#" item="key">
		<cfif emptyError[key] NEQ "">
			<cfset flag = 1/>
		</cfif>
	</cfloop>

	<cfif flag EQ 0>
		<!---Sending product details to the database --->
		<cfinvoke component = "component.productservice" method = "addProduct" returnVariable="check">
			<cfinvokeargument name="form" value=#form#>
		</cfinvoke>
		<cfif check EQ 1>
			<cfoutput>
				<h3 class="error_form">
					Please use a different Product Name.
				</h3>
			</cfoutput>
		<cfelse>
			<cfoutput>
				<h3 class="error_form">
					Product recorded!
				</h3>
			</cfoutput>
		</cfif>
	</cfif>

	<cfif #form.imageupload# NEQ "">
		<!---invoke function--->
		<cfset variables.addImage = dbObject.addImage(form.productName,form.imageUpload)/>

	</cfif>

</cfif>


<cfif structKeyExists(url,"getCategories") OR structKeyExists(url,"getCategory")>
		<cfset variables.returnCategory = dbObject.getCategory()/>
		<cfset variables.categoryJson = SerializeJSON(returnCategory)/>
		<cfoutput>#categoryJson#</cfoutput>
</cfif>

<cfif structKeyExists(url,"handleCategories")>
		<cfset variables.returnSubCategory = dbObject.handleCategories("#url.Site#")/>
		<cfset variables.subCategoryJson = SerializeJSON(returnSubCategory)/>
		<cfoutput>#subCategoryJson#</cfoutput>
</cfif>


